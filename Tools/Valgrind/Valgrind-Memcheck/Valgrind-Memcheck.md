This is the default `valgrind` tool that is run if another tool is not specified via `--tool`.

Quick Start Guide: https://valgrind.org/docs/manual/quick-start.html
Manual: https://valgrind.org/docs/manual/mc-manual.html

#### Purpose
This tool detects various kinds of memory errors in your program:
- Memory leaks
- Reads/Writes outside of allocated memory
- Uses of uninitialized values
- And more (see the manual section 4.1)

> If you're debugging an issue reported by `valgrind`, take a look at the section in the manual for that error type. They provide some helpful tips and additional command line options to give you more context.


#### Recommended default options
```sh
valgrind --leak-check=yes --track-origins=yes
```

#### Prerequisites
- Compile your program with `-g`

#### Why bother tracking memory leaks at-exit if the OS will reclaim all of the memory anyways?
Because `valgrind` doesn't just report the memory leaks, it categorizes them. Leaks that are "still reachable" aren't a problem, but leaks that are "definitely lost" or "possibly lost" in long running programs (servers) probably indicate a leak that could continuously gobble up memory until the OS kills the process. See section 4.2.10 in the manual for a break-down.