# Committing Local Changes
```mermaid
flowchart LR
 subgraph new_files["New Files"]
        untracked("Untracked<br>")
  end
 subgraph existing_files["Existing Files"]
        unmodified("Unmodified<br>")
        edits>"Edits<br>"]
        modified("Modified<br>")
  end
    staged("Staged<br>") --> commit>"git commit<br>"]
    commit --> committed("Committed<br>")
    unmodified --> edits
    edits --> modified
    modified --> add>"git add *<br>"]
    add --> staged
    untracked --> add
    style untracked stroke-width:2px,stroke-dasharray: 0,stroke:#D50000,fill:#FFCDD2,color:#000000
    style unmodified stroke-width:2px,stroke-dasharray: 0,stroke:#FF6D00,fill:#FFE0B2,color:#000000
    style edits stroke:#2962FF,fill:#FFF9C4,color:#000000
    style modified stroke-width:2px,stroke-dasharray: 0,stroke:#FFD600,fill:#FFF9C4,color:#000000
    style staged stroke-width:2px,stroke-dasharray: 0,stroke:#00C853,fill:#C8E6C9,color:#000000
    style commit stroke:#2962FF,fill:#FFF9C4,color:#000000
    style committed stroke-width:2px,stroke-dasharray: 0,stroke:#2962FF,fill:#BBDEFB,color:#000000
    style add stroke:#2962FF,fill:#FFF9C4,color:#000000
    style new_files stroke:#000000,fill:#757575,color:#FFFFFF
    style existing_files stroke:#000000,fill:#757575,color:#FFFFFF
```

# Reverting Local Changes
```mermaid
flowchart RL
 subgraph new_files["New Files"]
        untracked("Untracked<br>")
        delete>"Delete<br>"]
  end
 subgraph existing_files["Existing Files"]
        modified("Modified<br>")
        checkout>"git checkout &lt;file&gt;<br>"]
        unmodified("Unmodified<br>")
  end
    committed("Committed<br>") --> reset_mixed>"git reset --mixed HEAD~1<br>"] & reset_soft>"git reset --soft HEAD~1<br>"] & reset_hard>"git reset --hard HEAD~1<br>"]
    reset_mixed --> modified
    reset_soft --> staged("Staged<br>")
    staged --> reset>"git reset &lt;file&gt;<br>"]
    reset --> modified & untracked
    untracked --> delete
    modified --> checkout
    checkout --> unmodified
    reset_hard --> prev_commit("Previous Commit<br>")
    style untracked stroke-width:2px,stroke-dasharray: 0,stroke:#D50000,fill:#FFCDD2,color:#000000
    style delete stroke:#2962FF,fill:#FFF9C4,color:#000000
    style modified stroke-width:2px,stroke-dasharray: 0,stroke:#FFD600,fill:#FFF9C4,color:#000000
    style checkout stroke:#2962FF,fill:#FFF9C4,color:#000000
    style unmodified stroke-width:2px,stroke-dasharray: 0,stroke:#FF6D00,fill:#FFE0B2,color:#000000
    style committed stroke-width:2px,stroke-dasharray: 0,stroke:#2962FF,fill:#BBDEFB,color:#000000
    style reset_mixed stroke:#2962FF,fill:#FFF9C4,color:#000000
    style reset_soft stroke:#2962FF,fill:#FFF9C4,color:#000000
    style reset_hard stroke:#2962FF,fill:#FFF9C4,color:#000000
    style staged stroke-width:2px,stroke-dasharray: 0,stroke:#00C853,fill:#C8E6C9,color:#000000
    style reset stroke:#2962FF,fill:#FFF9C4,color:#000000
    style prev_commit stroke-width:2px,stroke-dasharray: 0,stroke:#2962FF,fill:#BBDEFB,color:#000000
    style new_files stroke:#000000,fill:#757575,color:#FFFFFF
    style existing_files stroke:#000000,fill:#757575,color:#FFFFFF
```
