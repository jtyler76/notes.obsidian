# 2.1 Setting Up the Environment
...
## 2.1.1 Working with Code Examples
...
# 2.2 Writing "Hello World"
```nasm
global _start

section .data
message: db 'hello, world!', 10

section .text
_start:
	mov  rax, 1          ;; system call number
	mov  rdi, 1          ;; argument #1: file descriptor
	mov  rsi, message    ;; argument #2: memory address of data to write
	mov  rdx, 14         ;; argument #3: number of bytes to write
	syscall
```

## 2.2.1 Basic Input and Output
- The `write()` system call number is `1`
- The `stdout` file descriptor is `1` (`stdin` is 0, `stderr` is 2)

## 2.2.2 Program Structure
### Sections
Assembly programs are divided into *sections*. Some *sections* include:
- The `.text` section holds instructions to be executed.
- The `.data` section is for *global variables*.

Section names are case sensitive.

### Labels
Labels are readable names that can precede commands.

The `_start` label is required as it is the starting point of the program. It also needs to be declared `global`.

### Instructions
Instructions are translated directly into machine code, and are defined by the assembly language of the processor.

Instructions are not case sensitive.

### Directives
Directives are not instructions, but are included in the program as a means to control the translation process into machine code. Some examples include; `global`, `section`, and `db`.

#### The `db`/`dw`/`dd`/`dq` Directives
These directives are used to create *byte data* of various sizes:
- `db` creates one or more pieces of data `1` byte each
- `dw` creates one or more pieces of data `2` bytes each
- `dd` creates one or more pieces of data `4` bytes each
- `dq` creates one or more pieces of data `8` bytes each

Examples:
```nasm nums
section .data
	example1: db 5, 16, 8, 4, 2, 1
	example2: dw 999
```

Note that the `example1` label is the address of the first element in the list (which I think you can think of as a static array of bytes).

Note also that you can create data in any section.

#### The `times` Directive
This directive can be used to repeat a given *directive* or *instruction* a given number of times:
```nasm nums
section .data
	example: times 999 db 42
```

In this case the `example` label is the address of the first element of a sequence of `999` bytes each with the value `42`.

## 2.2.3 Basic Instructions
#### `mov`
This instruction is used to write data to/from a *register* or *memory*. The following restrictions apply:
- `mov` cannot copy data from *memory* to *memory*
- The source and destination must be the same size

#### `syscall`
This instruction is used to run system calls. You need to set up some information in some registers prior to calling `syscall`:
- The `rax` register holds the *system call number*
- The arguments to the system call are held in the following registers: `rdi`, `rsi`, `rdx`, `r10`, `r8`, and `r9`. Note that system calls can only accept up to 6 arguments.

Note that the `syscall` instruction changes `rcx` and `r11`, which will be explained later.

Example:
```nasm nums
message: db 'hello, world!', 10
mov  rax, 1          ;; set the syscall number to 1, which corresponds to 'write()'
mov  rdi, 1          ;; arg #1: file descriptor of 1 (stdout)
mov  rsi, message    ;; arg #2: set the buffer address
mov  rdx, 14         ;; arg #3: length in bytes of the buffer
syscall
```

#### Building and Running the *Hello World* Example
Here's a refresh of the *Hello World* example:
```nasm nums
section .data
message: db 'hello, world!', 10

section .text
global _start
_start:
	mov  rax, 1          ;; write() syscall number
	mov  rdi, 1          ;; argument #1: file descriptor
	mov  rsi, message    ;; argument #2: memory address of data to write
	mov  rdx, 14         ;; argument #3: number of bytes to write
	syscall
```

To build the program:
```bash num
nasm -f elf64 -o hello.o hello.asm
ld -o hello hello.o
```

To run the program:
```
$ ./hello
[1]    27317 segmentation fault (core dumped)  ./hello
$
```

The program crashes because we didn't explicitly exit the program. This means that the CPU is trying to execute whatever is next on the stack (pointed to by the `rip` register), which is most likely garbage, resulting in a crash. Running the `exit()` system call to cleanly exit the program solves this problem.

```nasm nums
section .data
message: db 'hello, world!', 10

section .text
global _start
_start:
	mov  rax, 1          ;; write() syscall number
	mov  rdi, 1          ;; argument #1: file descriptor
	mov  rsi, message    ;; argument #2: memory address of data to write
	mov  rdx, 14         ;; argument #3: number of bytes to write
	syscall

	mov  rax, 60     ;; exit() syscall number
	xor  rdi, rdi    ;; argument #1: return code for program
	                 ;; clears the 'rdi' register (same as setting to 0). faster?
	syscall
```

Now the program runs perfectly!

# 2.3 Example: Output Register Contents
The following program is meant to output the contents of the `rax` register in hexadecimal format:
```nasm nums
...
```
## 2.3.1 Local Labels
...
## 2.3.2 Relative Addressing
...
## 2.3.3 Order of Execution
...
# 2.4 Function Calls
...
# 2.5 Working with Data
...
## 2.5.1 Endianness
...
## 2.5.2 Strings
...
## 2.5.3 Constant Precomputation
...
## 2.5.4 Pointers and Different Addressing Types
...
# 2.6 Example: Calculating String Length
...
# 2.7 Assignment: Input/Output Library
...
## 2.7.1 Self-Evaluation
...

# Questions
##### 11. What does the instruction `xor rdi, rdi` do?
This instruction clears the contents of the `rdi` register. My question is why this may be favoured over. Apparently there are a few reasons:
- ***Performance*** - Apparently `xor` is usually faster than `mov`. Furthermore some processors have a dedicated instruction for `xor`'ing  a register with itself which would be even faster.
- ***Smaller Code Size*** - Apparently the code size is larger if it has to account for moving things to/from registers which is avoided with the `xor` instruction.
- ***Setting Flags*** - The `xor` operation affects the processors flags such as the `ZF` (Zero Flag) allowing for uses in branching code.

##### 12.  What is the program return code?
When you run a program, it returns a numeric error code which will often indicate the success or failure or the program.

##### 13. What is the first argument of the `exit` system call?
The first argument to the `exit()` system call is the desired return code for the program.

##### 14.  Check that the ASCII codes mentioned in *Example 2.3* are correct.

##### 15.  What is the difference between the `sar` and `shr` instructions?

##### 16.  How do you write numbers in different number systems in a way understandable to NASM? Check the NASM documentation.

##### 17. What is the difference between the `je` and `jz` instructions?

##### 18.  What is `test` equal to after each of the commands listed in *Section 2.5.4*?

##### 19.  Can you spot a bug or two in *Listing 12-15*? When will they occur?

##### 20.  Try to rewrite `print_newline` in the assignment without calling `print_char` or copying its code. Hint: read about *tail call optimisation*.

##### 21.  Try to rewrite `print_int` without calling `print_uint` or copying its code. Hint: read about *tail call optimisation*.

##### 22.  Try to rewrite `print_int` without calling `print_uint`, copying its code, or using `jmp`. You will only need one instruction and a careful code placement. Read about *co-routines*.

##### 23. What is the connection between `rax`, `eax`, `ax`, `ah`, and `al`?

##### 24. How do we gain access to the parts of `r9`?

##### 25. How can you work with a hardware stack? Describe the instructions you can use.

##### 26. Which of these instructions are incorrect and why?
```nasm
mov [rax], 0
cmp [rdx], bl
mov bh, bl
mov al, al
add bpl, 9
add [9], spl
mov r8d, r9d
mov r3b, al
mov r9w, r2d
mov rcx, [rax + rbx + rdx]
mov r9, [r9 + 8 * rax]
mov [r8 + r7 + 10], 6
mov [r8 + r7 + 10], r6
```

##### 27. Enumerate the callee-saved registers.

##### 28. Enumerate the caller-saved registers.

##### 29. What is the meaning of the `rip` register?

##### 30. What is the `SF` flag?

##### 31. What is the `ZF` flag?

##### 32. Describe the effects of the following instructions:
- `sar`
- `shr`
- `xor`
- `jmp`
- `ja`, `jb` and friends
- `cmp`
- `mov`
- `inc`, `dec`
- `add`
- `imul`, `mul`
- `sub`
- `idiv`, `div`
- `call`, `ret`
- `push`, `pop`

##### 33. What is a label and does it have a size?

##### 34. How do you check whether an integer number is contained in a certain range (x,y)?

##### 35. What is the difference between `ja`/`jb` and `jg`/`jl`?

##### 36. What is the difference between `je` and `jz`?

##### 37. How do you test whether `rax` is zero without the `cmp` command?

##### 38. What is the program return code?

##### 39. How do we multiply `rax` by 9 using exactly one instruction?

##### 40. By using exactly two instructions (the first being `neg`), take an absolute value of an integer stored in `rax`.

##### 41. What is the difference between little and big endian?

##### 42. What is the most complex type of addressing?

##### 43. Where does the program execution start?

##### 44. `rax = 0x112233445567788`. We have performed `push rax`. What will be the contents of the byte at address `[rsp+3]`?










