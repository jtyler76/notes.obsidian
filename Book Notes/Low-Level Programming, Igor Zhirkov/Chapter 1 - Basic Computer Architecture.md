# 1.1 The Core Architecture
## 1.1.1 Model of Computation
...
## 1.1.2 von Neumann Architecture
...
# 1.2 Evolution
## 1.2.1 Drawback of von Neumann Architecture
...
## 1.2.2 Intel 64 Architecture
...
## 1.2.3 Architecture Extensions
- **Registers** - Memory cells directly on the CPU chip, with fast accesses (a few CPU cycles) that doesn't use the **bus**.
- **Hardware Stack** - Implemented on top of memory using **instruction** and a **register**. Used in computations, variable storage, and in implementing function call sequence.
- **Interrupts** - Signals (internal or external) that interrupt the execution of a program. The CPU suspends the programs execution, saves registers, and begins executing a special routine for the specific situation.
- **Protection Rings** - A CPU is at any given time operation in one protection ring. **Ring 0** is the most privileged, allowing any and all instructions to be run. **Ring 3** is the safest ring, where most things run. Modern operating systems do not use **Ring 1** or **Ring 2**.
- **Virtual Memory** - An abstraction over physical memory, making it easier to divide up memory between processes, and isolate the memory of processes from each other.

**Intel 64 and IA-32 Architectures Software Developer's Manual**: https://www.intel.com/content/www/us/en/developer/articles/technical/intel-sdm.html
# 1.3 Registers
- Registers are based on **transistors**, while main memory is based on **condensers**.
## 1.3.1 General Purpose Registers
On 64-bit systems, there are 16 general purpose registers, named `r0` to `r15`. The first 8 of them have alias' that are almost always used in place of their real names.

| Name     | Alias | Description                                                                          | Notes               |
| -------- | ----- | ------------------------------------------------------------------------------------ | ------------------- |
| `r0`     | `rax` | The 'a' stands for "accumulator". Used implicitly by some instructions               |                     |
| `r1`     | `rcx` | The 'c' stands for "cycles". Used in loops.                                          |                     |
| `r2`     | `rdx` | The 'd' stands for "data". Stores data during I/O operations.                        |                     |
| `r3`     | `rbx` | The 'b' stands for "base". Was used for base addressing in earlier processors.       |                     |
| `r4`     | `rsp` | Stores the address of the topmost element of the hardware stack.                     | Never use directly. |
| `r5`     | `rbp` | The stack frame's base.                                                              | Never use directly. |
| `r6`     | `rsi` | Source index in string manipulation commands (e.g. `movsd`)                          |                     |
| `r7`     | `rdi` | Destination index in string manipulation commands (e.g. `movsd`)                     |                     |
| `r8-r15` | N/A   | Usually used to store temporal variables, some are used implicitly by some commands. |                     |

Each of these registers is 64-bit, but you can access the subsections of registers by modifying their names.

When using the names `r0`, ..., `r15`, you can add any of the following suffix's"
- `d` for "double word", which accesses the lower 32 bits (e.g. `r0d`)
- `w` for "word", which accesses the lower 16 bits (e.g. `r0w`)
- `b` for "byte", which accesses the lower 8 bites (e.g. `r0b`)

You can do the same using the register alias', but the method differs depending on the specific register.

When using the following register alias' (`rax`, `rbx`, `rcx`, `rdx`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rax` to `eax`).
- Remove the `r` prefix to access the lower 16 bits of the register (e.g. `rax` to `ax`).
- Remove the `r` prefix, and change the `x` suffix to a `h` to access the upper 8 bits of the lower 16 bits of the register (e.g. `rax` to `ah`).
- Remove the `r` prefix, and change the `x` suffix to a `l` to access the lower 8 bits of the lower 16 bits of the register (e.g. `rax` to `al`).

For the remaining 4 registers (`rsi`, `rdi`, `rsp`, `rbp`) cannot access the upper 8 bits of the lower 16 bits like you can for `rax`, `rbx`, `rcx`, `rdx`.

When using the following register alias' (`rsi`, `rdi`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rsi` to `esi`).
- Remove the `r` prefix in order to access the lower 16 bits of the register (e.g. `rsi` to `si`).
- Remove the `r` prefix and append a `l` suffix in order  to access the lower 8 bits of the register (e.g. `rsi` to `sil`).

When using the following register alias' (`rsp`, `rbp`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rsp` to `esp`).
- Remove the `r` prefix in order to access the lower 16 bits of the register (e.g. `rsp` to `sp`).
- Remove the `r` prefix and append a `l` suffix in order to access the lower 8 bits of the register (e.g. `rsp` to `spl`).

## 1.3.2 Other Registers
### The `rip` Register
This register stores the address of the next instruction to be executed. Every time an instruction is executed, `rip` is updated to point to the next instruction.

### The `rflags` Register
This register stores flags that reflect the current state of the program. You can access it's lower 32 bits with `eflags` or its lower 16 bits with `flags`.

### Multimedia Registers
There is a series of 128 bit registers that are often used for multimedia purposes with names ranging from `xmm0` to `xmm15`.

### Model Specific Registers
Some hardware models have their own registers that they've added on top of the standard set.

## 1.3.3 System Registers
These are registers that are only used by the OS. Some examples are listed below.
- `cr0`, `cr4` store flags related to different processor modes and virtual memory
- `cr2`, `cr3` are used to support virtual memory
- `cr8` (aliased to `tpr`) is used to perform a fine tuning of the interrupts mechanism
- `efer` is another flags register used to control processor modes and extensions
- `idtr` stores the address of the interrupt descriptors table
- `gdtr` and `ldtr` store the addresses of the descriptor tables
- `cs`, `ds`, `ss`, `es`, `gs`, `fs` are so-called **segment registers**

# 1.4 Protection Rings
In **long mode**, the current **protection ring** is stored in the lowest two bits of the `cs` registers, and duplicated in the same of the `ss` register. These are only changed when handling an interrupt or making a system call.

# 1.5 Hardware Stack
The **Hardware Stack** is a stack data structure implemented in memory with a register `rsp` and instructions `push` and `pop`. The stack grows from the highest virtual address in a processes memory down towards zero.

The `rsp` register stores the address of the topmost element in the stack (which has the lowest memory value since we grow down from the top).

The `push` instruction will do the following:
1. Decrease the value of the `rsp` register by 2, 4 or 8 depending on the argument size.
2. The argument is then stored at that address.

The `pop` instruction will do the following:
1. The topmost stack element is copied into the register/memory.
2. The `rsp` value is increased by the size of the argument.

If you call `pop` when the stack is empty, it will work, but you'll likely get garbage.

# Working with Intel Docs - Reading Instruction Descriptions
The *INSTRUCTION* section describes the instruction mnemonics and allowed operand types.
- **R** stands for any general purpose register
- **M** stands for *memory location*
- **IMM** stands for *immediate value* (i.e. an integer constant such as `42`)
- A number defines the operand size
- If only specific registers are allowed, they are named

Examples
- `push r/m16` means push a general purpose 16-bit register of a 16-bit number taken from memory into the stack.

# 1.6 Questions
1. Refer to section 3.4.3 of the first volume of [[intel64_manual.pdf]] to learn about the `rflags` register. What is the meaning of flags `CF`, `AF`, `ZF`, `OF`, and `SF`? What is the difference between `OF` and `CF`?
	- `CF` (bit 0), is called the **Carry Flag**. This bit is set if an arithmetic operation generated a carry or a borrow out of the most significant bit of the result; otherwise it is cleared. This indicates an overflow condition in unsigned integer arithmetic. It is also used in multiple-precision arithmetic.
	- `AF` (bit 4), is called the **Auxiliary Carry Flag**. This bit is set if an arithmetic operation generates a carry or a borrow out of bit 3 of the results; otherwise it is cleared. This flag is used in binary coded decimal arithmetic (BCD).
	- `ZF` (bit 6), is called the **Zero Flag**. This bit is set if the result is zero; otherwise it is cleared.
	- `OF` (bit 11), is called the **Overflow Flag**. This bit is set if the integer result is too large a positive number or two small a negative number (excluding the sign bit) to be stored in the result; otherwise it is cleared. This is used to indicate an overflow condition in signed (two's complement) arithmetic.
	- `SF` (bit 7) is called the **Sign Flag**. This bit is set to indicate the sign of a result. If it's set is indicates a negative value, if it's not set it indicates a positive value.
	- The `OF` and `CF` flags are similar in that they both indicate overflow conditions in the result of an arithmetic operation. The difference is that `CF` is used to indicate an overflow in unsigned arithmetic, whereas `OF` is used to indicate an overflow in signed arithmetic.

2. What are the key principles of von Neumann architecture?
	- The CPU consists of two components, the **Control Unit** and the **Arithmetic Logic Unit** (ALU).
	- The **control unit** fetches instructions from memory, and the ALU performs the computations.
	- Memory stores only bits
	- Memory stores both encoded instructions and data to operate on. There's no means to distinguish data from code; both are bit strings.
	- Memory is organised into cells that are labelled with indices.
	- Programs consist of instructions that are fetch one after another. They are executed sequentially unless a jump instruction is executed.

3. What are registers?
	- Registers are small storage cells built into the CPU. They are faster than main memory but more expensive (\$\$\$).

4. What is the hardware stack?
	- See previous section on the hardware stack.

5. What are interrupts?
	- Interrupts are signals used to interrupt a programs execution to trigger a interrupt handler, which is code that will execute at that event.

6. What are the main problems that the modern extensions to the von Neumann model are trying to solve?
	- The von Neumann model is not interactive, you have to manually manipulate memory.
	- The von Neumann model does not support multi-tasking.
	- In the von Neumann model, any process can run any instructions and access anything they want. This isn't safe or secure.

7. What are the main general purpose registers of *Intel 64*?
	- The main general purpose registers are `r0` to `r7` which have alias' that hint at their implicit usage by certain instructions. There are also registers `r8` to `r15` which don't have alias' but can be used the same way.

8. What is the purpose of the stack pointer?
	- The stack pointer stores the address of the topmost element of the hardware stack (i.e. address of the next instruction to be executed).

9. Can the stack by empty?
	- Yes the stack can be empty, and you can use the `pop` instruction while the stack is empty, but you'll likely get garbage back as a result.

10. Can we count elements in a stack?
	- Given what I've learned from the chapter, I believe you could accomplish this by repeatedly using the `pop` instruction to pop elements off of the hardware stack until the `rsp` register holds the same address as the `rbp` register. This is because the `rbp` registers holds the address of the base of the stack, and `rsp` holds the address of the topmost element in the stack. If they are the same it should mean that there are no elements. You would just have to keep a count of how many `pop`'s it would take until they are equivalent.
	- Interestingly the GitHub page for the book says that the answer is no, you cannot count the elements in the stack.