The **Obsidian Git** plugin works great for desktop, but fails with large repositories on mobile because it uses a JavaScript Git implementation that is trash.

I found the following article though that describes a way to accomplish the same using **Termux** on Android: https://lucidhacker.substack.com/p/setting-up-git-syncing-for-obsidian

**Termux** is no longer available on the Google Play Store, but you can download the APK right off of their GitHub repository: https://github.com/termux/termux-app

I ran the following steps once I installed **Termux**:
```bash
## Update Packages
apt update && apt upgrade

## Install the 'cronie' package. This contains the 'crond' service used to run
## commands on a schedule (to pull down Git changes to Obsidian vault).
pkg install cronie

## Install the 'termux-services' package, which gives you the ability to enable
## the 'crond' service.
pkg install termux-services
```

After that, I had to restart **Termux**.

Then I performed the following steps in **Termux**:
```bash
## Enable the 'crond' service.
sv-enable crond

## Install 'vim'.
pkg install vim
export EDITOR=vim

## Install the 'git' and 'openssh' packages.
apt install git openssh

## Configure your git name and email.
git config --global user.name "James Tyler"
git config --global user.email "jet.48@protonmail.com"

## Create SSH keys to be used for authentication with Gitlab.
ssh-keygen -t rsa -C "jet.48@protonmail.com"

## Setup storage so we can access '/storage/emulated/0' via '~/storage' symlink.
termux-setup-storage

## Clone the Obsidian vault.
git clone git@gitlab.com:jtyler76/notes.obsidian.git ~/storage/shared/documents/notes.obsidian
```

At this point I can successfully open the vault in the Obsidian app, but without Git synching. Note that since my vault has the **Obsidian Git** plugin enabled, it will continually try and fail to `git pull`. So I had to go in the plugin settings on the mobile app, and disable the **Obsidian Git** plugin only on the mobile device. I'll manage the synching myself.

One method mentioned in the article uses `termux-job-scheduler`, which I'll try.

```bash
## Install 'termux-job-scheduler'.
pkg install termux-api
```

Create the following script at `~/obsidian_sync.sh`:
```bash
#!/bin/bash

cd ~/storage/shared/documents/notes.obsidian
git pull --rebase
git add --all
git commit -m "mobile update $(date)"
git push origin main
```

Run the following to set up the recurring job to run every 15 minutes:
```bash
termux-job-scheduler -s $HOME/obsidian_sync.sh --persisted true --period-ms 900000 --job-id 1   
```

Didn't work, complained about dubious ownership, and suggested the following:
```bash
git config --global --add safe.directory /storage/emulated/0/documents/notes.obsidian
```

Ran into another issue, some files insist on remaining changed but unable to be staged. So I had to go back to using normal `git pull` instead of with a `rebase`.

Turns out the `termux-job-scheduler` doesn't work... So instead I'm going back to `crond`.

Run `crontab -e` and add the following (every 5 minutes):
```bash
*/5 * * * * bash ~/obsidian_sync.sh
```

Everything seems to be working properly at that point!

Sometimes the sync works with **Termux** closed, sometimes it does. It's not hard to open **Termux** and run the script when needed though.