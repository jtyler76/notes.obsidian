# Primer
This enumeration is a common type for a function to return, as it allows you to indicate success or failure as well as data on success or error code on failure all in one return type.
```rust nums
pub enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

Example:
```rust
fn foo(x: u32) -> Result<u32, &'static str> {
	match x {
		0..=5  => Ok(x * 2),
		6..=10 => Err("invalid number"),
		_      => Ok(x),
	}
}

fn main() {
	match foo(3) {
		Ok(v)  => println!("Your value is {v}"),
		Err(e) => println!("Error: {e}"),
	}
}
```

Note that there is a really cool feature in the ***`?`*** operator when it comes to ***`Result<T,E>`***'s:
```rust
fn foo(x: u32) -> Result<u32, &'static str> {
	match x {
		0..=5  => Ok(x * 2),
		6..=10 => Err("invalid number"),
		_      => Ok(x),
	}
}

fn bar() -> Result<(), &'static str> {
    println!("value = {}", foo(3)? + foo(7)? + foo(11)?);
    Ok(())
}

fn main() {
    if let Err(e) = bar() {
        println!("Error: {e}")
    }
}
/* 
-> cargo run
   Compiling tmp v0.1.0 (/tmp/tmp.c46dLldNc0/tmp)
    Finished dev [unoptimized + debuginfo] target(s) in 0.32s
     Running `target/debug/tmp`
Error: invalid number                        
->
*/
```
The above is dumb code, but demonstrates how the ***`?`*** operator works. It allows you to forgo manual error checking for each call when you know you'd just want to propagate the ***`Err()`*** to the caller. 

Note here that ***`foo(7)`*** fails and the associated ***`Err("invalid number")`*** is returned by ***`bar()`***.

# Docs
- ***[`std::result`](https://doc.rust-lang.org/std/result/#enums)***  Module Documentation
	- Gives a nice overview of the ***`Result<T,E>`*** enum.
- ***[`Result<T,E>`](https://doc.rust-lang.org/std/result/enum.Result.html)*** Enum Documentation
	- Complete details of the enum and all associated methods.