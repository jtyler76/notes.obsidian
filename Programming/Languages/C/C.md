***Language Type***: Imperative

# Standards & References
The *C Standard* is defined by the *International Organization for Standardization (ISO)* and *American National Standards Institute (ANSI)*.
- A free online version of the latest (`C17`) standard can be found on the C committee's website here: https://www.open-std.org/jtc1/sc22/wg14/www/projects#9899
	- The ISO website links require payment, but the `N...` links do not.