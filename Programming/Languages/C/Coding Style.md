# Functions
```c
/**
 * @brief A brief sentence describing the function.
 *
 * @details If warranted, a paragraph describing the functions purpose in
 *          more detail.
 *
 * @param[in]      str    A sentence describing this input parameter.
 * @param[in,out]  size   A sentence describing this input and output parameter.
 *
 * @return @c EOK on success, or a standard @c errno on failure.
 */
static int
foo(char   const * const str,
    size_t       * const size)
{
    int ret = EOK;

    /* do some work... */

exit:
    return ret;
}
```
- Note the `doxygen` comment format. `doxygen` docs: https://www.doxygen.nl/index.html
- Note the 4 space indentation
- Note the return type on the preceding line
- Note the `const` after the type
- Note that `const` is used as frequently as it makes sense
- Note the Allman/BSD style brace placement
- Note the use of a returned standard `errno` rather than setting `errno`
- Note the use of `goto` statements for error and cleanup handling
- Note each function parameter is on a new line and lined up with the first one
- Note the lack of indentation of the `goto` label(s).
- TODO: re-evaluate the style and these notes


# IF Statements
```c
/* condition comment */
if ( (x == 1) || (x == 2) ) {
    // do some work...
}
/* condition comment */
else if ( x == 3 ) {
    // do some work...
}
/* condition comment */
else {
    // do some work...
}
```
- Note the K&R style brace placement
- Note the `else if` and `else` statement are on new lines, giving a convenient space for a comment for each condition if warranted
- Note the leading and trailing spaces in the condition parentheses
- Note that if there are more than one condition, each has it's own parentheses
- TODO: re-evaluate the style and these notes


# Variables Declarations
```c
static int s_counter = 0;

int
main(int argc, char *argv[])
{
    int ret = EOK;

    if ( argc > 2 ) {
        int x = 5;
        int y = 6;
        printf("%d-%d\n", x, y);
    }

 exit:
    return ret;
}
```
- Note that variables are declared at the top of the scope in which they are used.
- Note that static (global) variables are prefixed with `s_`.
- TODO: re-evaluate the style and these notes


# `main()` Return Values
```c
int
main(int argc, char *argv[])
{
    int ret = EOK;

    // do some work...

 done:
    return (ret == EOK ? EXIT_SUCCESS : EXIT_FAILURE);
}
```
- TODO: re-evaluate the style and these notes