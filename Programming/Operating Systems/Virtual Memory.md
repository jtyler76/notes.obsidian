Virtual Memory abstraction over physical memory, making it easier to divide up memory between processes, and isolate the memory of processes from each other.

# Sources
- [[Chapter 1 - Basic Computer Architecture]]