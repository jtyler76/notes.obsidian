# Documentation
Intel 64 and IA-32 Architectures Software Developer's Manual
- Web: https://www.intel.com/content/www/us/en/developer/articles/technical/intel-sdm.html
- Local copy:  [[intel64_manual.pdf]]
## Reading Documentation for Instructions
The *INSTRUCTION* section describes the instruction mnemonics and allowed operand types.
- **R** stands for any general purpose register
- **M** stands for *memory location*
- **IMM** stands for *immediate value* (i.e. an integer constant such as `42`)
- A number defines the operand size
- If only specific registers are allowed, they are named

Examples
- `push r/m16` means push a general purpose 16-bit register of a 16-bit number taken from memory into the stack.

# General Purpose Registers
On 64-bit systems, there are 16 general purpose registers, named `r0` to `r15`. The first 8 of them have alias' that are almost always used in place of their real names.

| Name     | Alias | Description                                                                          | Notes               |
| -------- | ----- | ------------------------------------------------------------------------------------ | ------------------- |
| `r0`     | `rax` | The 'a' stands for "accumulator". Used implicitly by some instructions               |                     |
| `r1`     | `rcx` | The 'c' stands for "cycles". Used in loops.                                          |                     |
| `r2`     | `rdx` | The 'd' stands for "data". Stores data during I/O operations.                        |                     |
| `r3`     | `rbx` | The 'b' stands for "base". Was used for base addressing in earlier processors.       |                     |
| `r4`     | `rsp` | Stores the address of the topmost element of the hardware stack.                     | Never use directly. |
| `r5`     | `rbp` | The stack frame's base.                                                              | Never use directly. |
| `r6`     | `rsi` | Source index in string manipulation commands (e.g. `movsd`)                          |                     |
| `r7`     | `rdi` | Destination index in string manipulation commands (e.g. `movsd`)                     |                     |
| `r8-r15` | N/A   | Usually used to store temporal variables, some are used implicitly by some commands. |                     |

Each of these registers is 64-bit, but you can access the subsections of registers by modifying their names.

When using the names `r0`, ..., `r15`, you can add any of the following suffix's"
- `d` for "double word", which accesses the lower 32 bits (e.g. `r0d`)
- `w` for "word", which accesses the lower 16 bits (e.g. `r0w`)
- `b` for "byte", which accesses the lower 8 bites (e.g. `r0b`)

You can do the same using the register alias', but the method differs depending on the specific register.

When using the following register alias' (`rax`, `rbx`, `rcx`, `rdx`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rax` to `eax`).
- Remove the `r` prefix to access the lower 16 bits of the register (e.g. `rax` to `ax`).
- Remove the `r` prefix, and change the `x` suffix to a `h` to access the upper 8 bits of the lower 16 bits of the register (e.g. `rax` to `ah`).
- Remove the `r` prefix, and change the `x` suffix to a `l` to access the lower 8 bits of the lower 16 bits of the register (e.g. `rax` to `al`).

For the remaining 4 registers (`rsi`, `rdi`, `rsp`, `rbp`) cannot access the upper 8 bits of the lower 16 bits like you can for `rax`, `rbx`, `rcx`, `rdx`.

When using the following register alias' (`rsi`, `rdi`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rsi` to `esi`).
- Remove the `r` prefix in order to access the lower 16 bits of the register (e.g. `rsi` to `si`).
- Remove the `r` prefix and append a `l` suffix in order  to access the lower 8 bits of the register (e.g. `rsi` to `sil`).

When using the following register alias' (`rsp`, `rbp`), you can modify the alias' as follows:
- Replace the `r` prefix with `e` in order to access the lower 32 bits of the register (e.g. `rsp` to `esp`).
- Remove the `r` prefix in order to access the lower 16 bits of the register (e.g. `rsp` to `sp`).
- Remove the `r` prefix and append a `l` suffix in order to access the lower 8 bits of the register (e.g. `rsp` to `spl`).

# Other Registers
### The `rip` Register
This register stores the address of the next instruction to be executed. Every time an instruction is executed, `rip` is updated to point to the next instruction.

### The `rflags` Register
This register stores flags that reflect the current state of the program. You can access it's lower 32 bits with `eflags` or its lower 16 bits with `flags`.

### Multimedia Registers
There is a series of 128 bit registers that are often used for multimedia purposes with names ranging from `xmm0` to `xmm15`.

### Model Specific Registers
Some hardware models have their own registers that they've added on top of the standard set.

# System Registers
These are registers that are only used by the OS. Some examples are listed below.
- `cr0`, `cr4` store flags related to different processor modes and virtual memory
- `cr2`, `cr3` are used to support virtual memory
- `cr8` (aliased to `tpr`) is used to perform a fine tuning of the interrupts mechanism
- `efer` is another flags register used to control processor modes and extensions
- `idtr` stores the address of the interrupt descriptors table
- `gdtr` and `ldtr` store the addresses of the descriptor tables
- `cs`, `ds`, `ss`, `es`, `gs`, `fs` are so-called **segment registers**

# Protection Rings
In **long mode**, the current **protection ring** is stored in the lowest two bits of the `cs` registers, and duplicated in the same of the `ss` register. These are only changed when handling an interrupt or making a system call.

# Hardware Stack
The **Hardware Stack** is a stack data structure implemented in memory with a register `rsp` and instructions `push` and `pop`. The stack grows from the highest virtual address in a processes memory down towards zero.

The `rsp` register stores the address of the topmost element in the stack (which has the lowest memory value since we grow down from the top).

The `push` instruction will do the following:
1. Decrease the value of the `rsp` register by 2, 4 or 8 depending on the argument size.
2. The argument is then stored at that address.

The `pop` instruction will do the following:
1. The topmost stack element is copied into the register/memory.
2. The `rsp` value is increased by the size of the argument.

If you call `pop` when the stack is empty, it will work, but you'll likely get garbage.

# Sources
- [[Chapter 1 - Basic Computer Architecture]]