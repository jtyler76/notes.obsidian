# Registers
Registers are memory cells directly on the CPU chip, with fast accesses (a few CPU cycles) that doesn't use the *bus. Registers are based on *transistors* whereas main memory is based on *condensers*.

# Hardware Stack
The *Hardware Stack* is not technically part of the CPU, but it is an integral part of program execution and is managed by the CPU. The *Hardware Stack* behaves like a *Stack Data Structure*, implemented in main memory. The stack grows from the highest memory address in the programs memory space, and grows towards address 0.

The stack is used to manage local variables and the function call sequence.

# Interrupts
Interrupts are signals (internal or external) that interrupt the execution of a program. The CPU suspends the programs execution, saves registers, and begins executing a special routine for the specific situation.

# Protection Rings
A CPU is at any given time operation in one protection ring. **Ring 0** is the most privileged, allowing any and all instructions to be run. **Ring 3** is the safest ring, where most things run. Modern operating systems do not use **Ring 1** or **Ring 2**.

# Sources
- [[Chapter 1 - Basic Computer Architecture]]